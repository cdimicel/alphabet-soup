from setuptools import setup, find_packages

setup(
    name='alphabet_soup',
    version='0.1.0',    
    description='Enlighten coding project--alphabet soup',
    author='Charlene DiMiceli',
    author_email='cdimicel@umd.edu',
    packages=['alphabet_soup', 'alphabet_soup.test'],
    scripts=['./bin/given.py'],
    package_data={'': ['*.txt', '*.out']},
    install_requires=[
                      'numpy',
                      ],

)
